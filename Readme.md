## Install
[Install docker on your machine.][install-docker]

[Install docker-compose on your machine.][install-docker-compose]

Clone this repository.

``` bash
$ git clone git@gitlab.com:makarenkovalex/komitet-test.git
```

Switch to the cloned directory.

``` bash
$ cd komitet-test
```

Start the stack.

``` bash
$ docker-compose up
```

[install-docker]: https://docs.docker.com/engine/installation
[install-docker-compose]: https://docs.docker.com/compose/install
