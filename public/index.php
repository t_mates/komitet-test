<?php

use Aleksandr\KomitetTest\App;
use Aleksandr\KomitetTest\DependencyInjection\ContainerLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Response;

require '../vendor/autoload.php';

$loader = new ContainerLoader('../config', 'services.yaml', dirname(__DIR__));
$containerBuilder = $loader->configure();

$app = $containerBuilder->get('app');

$app->run();
