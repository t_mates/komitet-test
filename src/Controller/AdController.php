<?php

declare(strict_types=1);

namespace Aleksandr\KomitetTest\Controller;

use Aleksandr\KomitetTest\DTO\AdDTO;
use Aleksandr\KomitetTest\Entity\Advertisement;
use Aleksandr\KomitetTest\Service\AdCreator;
use Aleksandr\KomitetTest\Service\AdEditor;
use Aleksandr\KomitetTest\Service\AdFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdController {

    private AdCreator $adCreator;
    private Request $request;
    private AdEditor $adEditor;
    private AdFetcher $adFetcher;

    public function __construct(Request $request, AdCreator $adCreator, AdEditor $adEditor, AdFetcher $adFetcher)
    {
        $this->adCreator = $adCreator;
        $this->request = $request;
        $this->adEditor = $adEditor;
        $this->adFetcher = $adFetcher;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function create() {
        $ad = $this->adCreator->create($this->request);
        $this->sendOk($ad);
    }

    public function edit(string $id) {
        try {
            $ad = $this->adEditor->edit((int)$id, $this->request);
            $this->sendOk($ad);
        } catch (\Exception $e) {
            $this->sendKO($e);
        }
    }

    public function relevant() {
        try {
            $ad = $this->adFetcher->get($this->request);
            $this->sendOk($ad);
        } catch (\Exception $e) {
            $this->sendKO($e);
        }
    }

    private function sendOk(Advertisement $ad) {
        $adDto = new AdDTO($ad);
        $adDto->setCode(Response::HTTP_CREATED);
        $adDto->setMessage('OK');
        header("Content-type: application/json");
        echo $adDto;
    }

    private function sendKO(\Exception $e) {
        $adDto = new AdDTO();
        $adDto->setCode(Response::HTTP_BAD_REQUEST);
        $adDto->setMessage($e->getMessage());
        header("Content-type: application/json");
        echo $adDto;
    }
}