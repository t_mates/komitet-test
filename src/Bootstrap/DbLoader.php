<?php
declare(strict_types=1);

namespace Aleksandr\KomitetTest\Bootstrap;

use Aleksandr\KomitetTest\Entity\Advertisement;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use PDO;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DbLoader {

    private string $projectDir;
    private ContainerBuilder $container;

    public function __construct(string $projectDir, ContainerBuilder $container)
    {
        $this->projectDir = $projectDir;
        $this->container = $container;
    }

    public function load()
    {
        $isDevMode = true;
        $proxyDir = null;
        $cache = null;
        $useSimpleAnnotationReader = false;
        $config = Setup::createAnnotationMetadataConfiguration([$this->projectDir.'/src/Entity'], $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

        $conn = array(
            'driver'   => 'pdo_sqlite',
            'path' => $this->projectDir . '/var/db.sqlite'
        );

        $em = EntityManager::create($conn, $config);


//        $em2 = $this->container->get('entity_manager');
//        var_dump(get_class($em2));
//        die();
    }
}