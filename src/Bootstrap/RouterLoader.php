<?php
declare(strict_types=1);

namespace Aleksandr\KomitetTest\Bootstrap;

use Aleksandr\KomitetTest\Controller\AdController;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Router;

class RouterLoader {

    private string $path;
    private string $filename;
    private ContainerInterface $container;
    private $controller;
    private $method;
    private $params;

    public function __construct(string $path, string $filename, ContainerInterface $container)
    {
        $this->path = $path;
        $this->filename = $filename;
        $this->container = $container;
    }

    public function load()
    {
        $context = new RequestContext();
        $context->fromRequest(Request::createFromGlobals());

        $router = new Router(
            new YamlFileLoader(new FileLocator($this->path)),
            $this->filename,
            [],
            $context
        );

        $params = $router->match($context->getPathInfo());

        [$class, $method] = explode('::', $params['_controller'], 2);
        $controller = $this->container->get($params['_route']);
        // remove "_controller" and "_method" parameters and leave only "real" parameters like "id"
        $params = array_filter($params, function ($key) { return mb_substr($key, 0, 1) !== '_'; }, ARRAY_FILTER_USE_KEY);
        $this->controller = $controller;
        $this->method = $method;
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }
}