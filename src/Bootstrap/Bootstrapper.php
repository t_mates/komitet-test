<?php
declare(strict_types=1);

namespace Aleksandr\KomitetTest\Bootstrap;

use Aleksandr\KomitetTest\Entity\Advertisement;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Tools\SchemaTool;

class Bootstrapper {

    private RouterLoader $routerLoader;
    private DbLoader $dbLoader;

    public function __construct(RouterLoader $routerLoader, DbLoader  $dbLoader)
    {
        $this->routerLoader = $routerLoader;
        $this->dbLoader = $dbLoader;
    }

    public function run(): RouterLoader
    {
        $this->routerLoader->load();
        $this->dbLoader->load();

        return $this->routerLoader;
    }
}