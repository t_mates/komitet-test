<?php
declare(strict_types=1);

namespace Aleksandr\KomitetTest\DTO;

use Aleksandr\KomitetTest\Entity\Advertisement;

class AdDTO {

    private ?Advertisement $advertisement;

    public function __construct(Advertisement $advertisement = null)
    {
        $this->advertisement = $advertisement;
        $this->data = new DataDTO($advertisement);
    }

    /** @var string */
    public string $message;
    /** @var int */
    public int $code;
    /** @var DataDTO */
    public DataDTO $data;

    /**
     * @return Advertisement
     */
    public function getAdvertisement(): Advertisement
    {
        return $this->advertisement;
    }

    /**
     * @param Advertisement $advertisement
     */
    public function setAdvertisement(Advertisement $advertisement): void
    {
        $this->advertisement = $advertisement;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code): void
    {
        $this->code = $code;
    }

    /**
     * @return DataDTO
     */
    public function getData(): DataDTO
    {
        return $this->data;
    }

    /**
     * @param DataDTO $data
     */
    public function setData(DataDTO $data): void
    {
        $this->data = $data;
    }

    public function __toString()
    {
        return json_encode($this);
    }
}