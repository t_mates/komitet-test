<?php
declare(strict_types=1);

namespace Aleksandr\KomitetTest\DTO;

use Aleksandr\KomitetTest\Entity\Advertisement;

class DataDTO {
    private ?Advertisement $advertisement;

    public function __construct(Advertisement $advertisement = null)
    {
        $this->advertisement = $advertisement;
        if ($this->advertisement !== null) {
            $this->id = $advertisement->getId();
            $this->banner = $advertisement->getBanner();
            $this->text = $advertisement->getText();
        }
    }

    /** @var int */
    public int $id;
    /** @var string */
    public string $banner;
    /** @var string */
    public string $text;
}