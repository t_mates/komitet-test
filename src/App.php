<?php
declare(strict_types=1);

namespace Aleksandr\KomitetTest;

use Aleksandr\KomitetTest\Bootstrap\Bootstrapper;
use Aleksandr\KomitetTest\Controller\AdController;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class App {

    private Bootstrapper $bootstrapper;

    public function __construct(Bootstrapper  $bootstrapper, Response $response)
    {
        $this->bootstrapper = $bootstrapper;
    }

    public function run(): void
    {
        $router = $this->bootstrapper->run();

        call_user_func_array([$router->getController(), $router->getMethod()], $router->getParams());
    }
}