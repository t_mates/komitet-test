<?php
declare(strict_types=1);

namespace Aleksandr\KomitetTest\Service;

use Aleksandr\KomitetTest\Entity\Advertisement;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

class AdEditor {
    private EntityManager $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function edit(int $id, Request $request): Advertisement
    {
        $repo = $this->entityManager->getRepository(Advertisement::class);
        $ad = $repo->find($id);

        if ($ad === null) {
            throw new \Exception("ad with id: $id not found");
        }

        $ad->setText($request->get('text'));
        $ad->setPrice((int)$request->get('price'));
        $ad->setLimits((int)$request->get('limit'));
        $ad->setBanner($request->get('banner'));

        $this->entityManager->flush();

        return $ad;
    }
}